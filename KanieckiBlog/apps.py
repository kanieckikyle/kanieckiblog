from django.contrib.admin.apps import AdminConfig


class KanieckiBlogAdminConfig(AdminConfig):
    default_site = "KanieckiBlog.admin.KanieckiBlogAdminSite"
