import re
from django.http import QueryDict


class PrepareQueryParametersMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Code to be executed for each request before
        # the view (and later middleware) are called.

        request.pGET = self.prepare_parameters(request.GET)
        response = self.get_response(request)

        # Code to be executed for each request/response after
        # the view is called.

        return response

    def prepare_parameters(self, params: QueryDict) -> dict:
        """
        Takes in a QueryDict of parameters from a request object
        (`request.GET`) and transforms it into primitive python values.

        This is needed because Django keeps `true` and `false` values from
        javascript as strings in the querydict. But for easy filtering on querysets,
        we change things into python primatives instead. Arrays are included as well.

        Args:
            params (:class:`~django.http.QueryDict`):
                The parameters for filtering a queryset sent to us
                from the user in a Request

        Returns:
            dict: A new dictionary with all values as python's version

            For Example, if the user sends the following `GET` request:

            .. code-block:: rest

                https://kylekaniecki.com/v1/teams/?title=test&id=1&id=2&id=3&verified=true


            This function would return::

                {
                    "title": "test",
                    "id": [1, 2, 3],
                    "verified": True
                }

        """
        array_regex = re.compile(r"(?P<key>[a-zA-Z_]+)\[(?P<index>\d+)\]")

        # This converts a query dict into a normal dictionary
        # However, it preserves lists if a list of query parameters is given
        params = {k: v[0] if len(v) == 1 else v for k, v in params.lists()}
        new_params = {}

        def clean_value(value):
            if isinstance(value, list):
                return list(map(clean_value, value))
            try:
                return int(value)
            except (ValueError, TypeError):
                return value

        for key, value in params.items():
            arr_match = array_regex.match(key)
            if arr_match:
                group_dict = arr_match.groupdict()
                arr_key = group_dict.get("key")
                if arr_key not in new_params:
                    new_params[arr_key] = []
                new_params[arr_key].append(clean_value(value))
            elif value in ["true", "True"]:
                new_params[key] = True
            elif value in ["false", "False"]:
                new_params[key] = False
            else:
                new_params[key] = clean_value(value)

        return new_params
