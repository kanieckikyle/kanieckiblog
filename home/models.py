from django.db import models

from wagtail.core.models import Page


class HomePage(Page):
    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request, *args, **kwargs)

        context["kaniecki_skills"] = [
            "Django",
            "Angular 2+",
            "PostgreSQL",
            "Docker",
            "Kubernetes",
            "Celery Task Queue",
            "Redis",
            "Nginx",
            "React",
            "Amazon Web Services",
        ]

        return context
